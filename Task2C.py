from floodsystem import stationdata
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list

stations = build_station_list()
stationdata.update_water_levels(stations)

top_10 = stations_highest_rel_level(stations, 10)

for i in top_10:
    print(i.name + " " + str(i.relative_water_level()))