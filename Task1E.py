# Import the relevent functions from their files and folders
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

# 'stations' is a variable which contains the info in build_station_list
stations = build_station_list()

# Top 9 rivers with most largest amount of stations
number = rivers_by_station_number(stations, 5)

# Print
print(number)
