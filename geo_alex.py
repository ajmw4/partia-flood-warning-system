# Import geo submodule
from floodsystem import geo

# Import build_station_list function
from floodsystem.stationdata import build_station_list

# Import random number generator
import random

# Import stationdata submodule
from floodsystem import stationdata


def test_stations_within_radius():
    """ testing the function in 1C stations_within_radius"""

    clare_college = (52.2051,0.1152)
    # coords of clare college 
 
    r = 10 

    stations = build_station_list()
    # 'generate the large list of stations and their data 

    stations_in_range = geo.stations_within_radius(stations,clare_college,r)
    # create a list of stations within 10km of clare college and their respsective distance to test on 
    stations_in_range.sort()


    assert type(stations_in_range) == type([])
    assert type(stations_in_range[0][0]) == type("a")
    assert type(stations_in_range[0][1]) == type(0.0)

    # check that the fuction produces a list where the first element is a string (the station) and the second a float (the distance)


def test_stations_by_river():
    """ testing the 1D function stations by river"""

    stations = build_station_list()

    test = geo.stations_by_river(stations)

    # import dictionary of rivers and their corresponding stations 

    random_number = random.randint(0,len(test)-1)

    # creates a random number within the range of the length of the dictionary 

    assert stations[random_number].name in test[stations[random_number].river]

    # checks if this random station is in the dictionary as the value for the river that it is on (the relevant key)