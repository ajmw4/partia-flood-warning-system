from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river

stations = build_station_list()


rwstation = list(rivers_with_station(stations))
print(rwstation[:10])


stationsonriver = stations_by_river(stations)

print("River Aire" + str(stationsonriver["River Aire"]))

print("River Cam " + str(stationsonriver["River Cam"]))

print("River Thames" + str(stationsonriver["River Thames"]))

