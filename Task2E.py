# Import the relevent functions from thier files and folders
import matplotlib.pyplot as plt
import datetime
from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels

# Create a list of stations
stations = build_station_list()

# Update to current water levels
update_water_levels(stations)

# Create a tuple of the 5 stations with the highest level compared to the typical values, and the water height
stations_highest = stations_highest_rel_level(stations, 5)

# Set length of time being measured to 10 days
dt = 10

# For each of the 5 highest stations
for station in stations_highest:

    # Retrieving level data over the past 10 days
    dates, levels = fetch_measure_levels(station.measure_id,
                                    dt=datetime.timedelta(days=dt))

    # Create a graph of the retrieved data
    plot_water_levels(station, dates, levels)

    # Show the graph
    plt.show()