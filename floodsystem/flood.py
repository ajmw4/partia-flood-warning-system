from floodsystem.utils import sorted_by_key
from floodsystem.stationdata import update_water_levels

# imports relevent modules for usage



def stations_level_over_threshold(stations, tol):
    """ returs a list of tuples containing a station with a relative water level over 
        a given tolerence and the water level at each station, this list is sorted in order 
        of decreasing water level"""

    list_over_tol = []
    # sets up list for tuples to be added to 

    for i in stations:
        if i.relative_water_level() == None:
            continue

        # iterates through all the stations and passes over the ones that have no data for relative water levels
    
        if i.relative_water_level() > tol : 

            tple = (i, i.relative_water_level())

 
            list_over_tol.append(tple)

            # sets up tuple of each station and its relative water level for all stations
    
    sorted_list_over_tol = sorted_by_key(list_over_tol, 1, True)

    # sorts list in descending order based on the numerical values of water level ( [1])

    return sorted_list_over_tol


def stations_highest_rel_level(stations, N):
    """ returns a list of N stations that have the highest current relative water level as compared to their typical range
        this list is sorted in descending order"""

    list_of_N_stations = []

    # sets up list for the list of high risk stations will be added to 

    stations = [stat for stat in stations if stat.relative_water_level() != None]

    # removes stations which have None as their value for relative water level by using list comprehension

    for i in range(N):

        if len(stations) == 0:
            break 
            # accounts for the situation in which the list has no entries
       
        first_station = stations[0]

        # starts by setting the first station to the highest station in the list 

        
        for j in stations:

            if j.relative_water_level() >  first_station.relative_water_level():
                
                 first_station = j

                 # if the curent station is higher that the highest previous one it sets the current station to the highest value 
               
        stations.remove( first_station)
       # removes the highest station in the first iteration from the list of stations

        list_of_N_stations.append(first_station)
        # then adds this station to the list of N stations 

    return list_of_N_stations

