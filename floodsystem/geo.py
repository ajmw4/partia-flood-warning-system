# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data."""

from floodsystem.utils import sorted_by_key  # noqa

# Import the haversine function
from haversine import haversine, Unit




def stations_by_distance(stations, p):
    '''This function creates and returns a list of all the stations as a tuple with thier distance from a point
    in order from closest to furthest.'''

    # Creating empty list
    d_list = []

    # For all i stations
    for i in range(0, len(stations)-1):
        
        # Add station i to the end of the list as a tuple of 
        # the stations name and its distance from p calculated
        #  by the haversine function
        d_list.append((stations[i].name, haversine(p, stations[i].coord)))
        
        # Sort the list alphabetically by the second entry
        # i.e. closest to furthest 
        d_list_sorted = sorted_by_key(d_list,1)
    
    # Return the sorted list of staiions in order from closest
    # to furthest 
    return d_list_sorted




def stations_within_radius(stations, centre, r):
    list_in_radius = []
    distance = 0 
    # set up empty list of stations within a given radius for the stations to be added to 
    for i in range(0,len(stations)-1):
        distance = haversine(centre, stations[i].coord, unit=Unit.KILOMETERS)
        # use the haversine function to create a list of distances from a centre point 
        if distance < r:
            list_in_radius.append(stations[i].name)
        list_in_radius.sort()
    return list_in_radius 
# if distance from centre is smaller than given r then add the station to the list of stations within a given radius 




def rivers_with_station(stations):
    """Given a list of stations, returns a set with the names of the rivers that have a monitoring station."""
   
   # create empty list for the rivers to be added to 
    rivers_in_stations = []

    for i in range (0,len(stations)-1):
        rivers_in_stations.append(stations[i].river)
    # create a list of all the rivers within each stations iterating over all the stations 
    
    set_of_rivers_in_stations = (set(rivers_in_stations))

    # remove all the duplicate rivers by implementing the set function, the "rivers_in_stations" needs to be defined as a list again 


    return set_of_rivers_in_stations




def stations_by_river(stations):
    """Returns a dictionary that maps river names (key) to a list of stations (value) on any given river."""

    dict_river = {}
    list_of_rivers = list(rivers_with_station(stations))
    list_of_rivers.sort()

    # set up a empty dictionary to add the river names ( the keys ) and the corresponding stations ( the values)
    # use previous function to build a list of rivers as a set 

    for river in list_of_rivers:
        list_of_stations = []
    #  create empty list so that stations can be added, then these will be added in the dictionary as the values 
        for i in range(0, len(stations)-1):
            if river == stations[i].river:
                list_of_stations.append(stations[i].name)
                # add stations to the list "list_of_stations" if the station matches the corresponding river, iterate over all the rivers 
            list_of_stations.sort()
            dict_river[river] = list_of_stations
            # adds the river as the keys and the list of stations on that river as the values to the dictionary 
    return dict_river




# Define the function
def rivers_by_station_number(stations, N):
    '''This function counts the number of stations on each river and sorts them by order of highest to lowest
    and returns the 9 highest, unless it is tied in which case it will add more to the list.'''

    # Create a list of just stations
    rivers = rivers_with_station(stations)

    # Create an empty list
    number_list = []

    # For each element in the rivers list
    for river in rivers:

        # Set a variable 'count' to 0
        count = 0 

        # For all i stations
        for i in range(0, len(stations)-1):

            # If the ith stations river is the river in question
            if stations[i].river == river:

                # Add one to the 'count'
                count = count + 1

        # Add river i to the end of the list as a tuple of
        # rhe rivers name and number of stations on it
        number_list.append((river, count))

        # Sort the number list in order from most to least
        number_list_sorted = sorted_by_key(number_list, 1, reverse=True)

    # If the penultimate count entry is the same as the final
    while number_list_sorted[N-1][1] == number_list_sorted[N][1]:

        # Add one to the list
        N = N + 1

    # Return the list of rivers sorted by number of stations on them, up to the correct number
    return number_list_sorted[:N]