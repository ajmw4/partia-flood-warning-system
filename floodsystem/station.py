# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d




    # Define the function
    def typical_range_consistent(self):
        '''This method finds out if the station has a consistant range of heights or not.'''

        # If the station hasn't got a typical range
        if self.typical_range == None:

            # Return false
            return False

        # If the stations low value is higher than its high value
        elif self.typical_range[0]>self.typical_range[1]:

            # Return false
            return False

        # If niether of the above apply
        else:

            # Return true
            return True

    
    def relative_water_level(self):


         if self.typical_range_consistent() == False :
             return None 
        
         if self.latest_level == None:
            return None 
        
         else:
            ratio = (self.latest_level - self.typical_range[0]) / (self.typical_range[1] - self.typical_range[0])
        
            return ratio











# Define the function
def inconsistent_typical_range_stations(stations):
    '''This function creates a list of all the stations that have inconsistant values in aphabetical order.'''

    # Create and empty list
    ic_list = []

    # For all stations in the list of stations
    for station in stations:

        # If the typical range function is false
        if station.typical_range_consistent() == False:

            # Add the station name to the end of the list
            ic_list.append(station.name)

            # Sort the list into alphabetical order
            ic_list.sort()

    # Return the sorted list
    return ic_list