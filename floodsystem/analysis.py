# Import all of the relevent libraries and modules
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

# Define the function
def polyfit(dates, levels, p):

    # Set variable x as the dates as floats
    x = matplotlib.dates.date2num(dates)

    # Set variable y as the levels
    y = list(levels)

    # Shift so that the first value is the starting value
    d0 = x[0]

    # Find coefficients of best-fit polynomial f(x) of degree p, offset by the first date to avoid floating point inaccuracies
    p_coeff = np.polyfit(x - d0, y, p)

    # Converting coefficent into a polymonial that can be evaluated
    poly = np.poly1d(p_coeff)

    #
    return (poly, d0)
    