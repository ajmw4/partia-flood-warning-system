# Import all of the relevent libraries and modules
import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from floodsystem.station import MonitoringStation
from floodsystem.analysis import polyfit


# Define the function
def plot_water_levels(station, dates, levels):

    # Set variable t as a list of dates
    t = list(dates)

    # Set variable level as a list of levels
    level = list(levels)

    # Set typical_high as the typical high value for the river
    typical_high = [station.typical_range[1]] * len(t)

    # Set typical_low as the typical low value for the river
    typical_low = [station.typical_range[0]] * len(t)

    # Plot the graph
    plt.plot(t, level)

    # Add the values for the typical low and high values
    plt.plot(t, typical_high)
    plt.plot(t, typical_low)

    # Label the x-axis as Date
    plt.xlabel('Date')

    # Label the y-axis as Water Level /m
    plt.ylabel('Water Level /m')

    # Rotate the date labels by 45 degrees
    plt.xticks(rotation=45)

    # Label the graph as the station name
    plt.title(station.name)

    # Make sure plot does not cut off date labels
    plt.tight_layout()


# Define the function
def plot_water_level_with_fit(stations, dates, levels, p):

    # Create a graph from the above function
    plot_water_levels(stations, dates, levels)

    # # Set variable x as the dates as floats
    x = matplotlib.dates.date2num(dates)

    # Find the offset and polynomial to regress the data
    polynomial, d0 = polyfit(dates, levels, p)

    # Plot the polynomial using the offset
    plt.plot(dates, polynomial(x - d0))