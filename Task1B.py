# Import the relevent functions from thier files and folders
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

# 'stations' is a variable which contains the info in build_station_list
stations = build_station_list()

# 'dist_list' is a variable which contains the info in stations_by_distance
# the distance of each station is being measure from the specified coordinates
dist_list = stations_by_distance(stations,(52.2053, 0.1218))

# Printing only the 10 closest and 10 furthest stations
print (dist_list[:10])
print (dist_list[-10:])
