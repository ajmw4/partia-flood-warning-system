# Import geo submodule
from floodsystem import geo

# Import build_station_list function
from floodsystem.stationdata import build_station_list

# Import random number generator
import random

# Import stationdata submodule
from floodsystem import stationdata

# 'stations' is a variable of the list of stations
stations = build_station_list()

# Define the function to test the function 'stations_by_distance'
def test_stations_by_distance():
    '''This function tests various aspects of the output of stations_by_distance
    to see if they make sense.'''

    # Define the coordinates of Clare College
    clare_coords = (52.2051, 0.1152)

    # Create a list of stations
    stations = stationdata.build_station_list()

    # Makes a list of stations from Clare College
    geo.stations_by_distance(stations, clare_coords)

    # Puts the list in order of furthest to closest from Clare College
    stations_sorted_by_distance = geo.stations_by_distance(stations, clare_coords)

    # Create two lists
    closest_stations = []
    furthest_stations = []

    # Put the first 10 stations in the sorted list into the closest list
    for i in range(9):
       closest_stations.append(stations_sorted_by_distance[i])

    # Put the last 10 stations in the sorted list into the furthest list
    furthest_stations = (stations_sorted_by_distance[-10:])

    # Checking to see if the furthest is greater than the smallest
    assert furthest_stations[0][1] > closest_stations[0][1]

    # Checking distances are floats
    assert type(closest_stations[0][1]) == type(furthest_stations[0][1]) ==type(0.0)

    # Checking to see if they are lists
    assert type(closest_stations) == type(furthest_stations) == type([])




# Define the function to test the function 'rivers_with_stations'
def test_rivers_with_stations():
    '''This function tests various aspects of the output of rivers_with_stations
    to see if they make sense.'''

    # Creating a set of rivers which has a station in it
    check = geo.rivers_with_station(stations)

    # Chcecking this is a set
    assert type(check) == type(set())

    # Changing this to a list
    check = list(check)

    # For all of check's items
    for i in range(len(check)):

        # Check that the number of rivers with stations is less
        # than the number of stations
        assert len(check) < len(stations)




# Define the function to test the function 'rivers_by_station_number'
def test_rivers_by_station_number():
    '''This function tests various aspects of the output of rivers_with_stations
    to see if they make sense.'''

    # Checking the return is a list
    assert type(geo.rivers_by_station_number(stations, 10)) == type([])

    # Checking if the length of the list is more than or the same as N
    # As rivers could have the same number of stations on them
    assert len(geo.rivers_by_station_number(stations, 18)) >= 18





def test_stations_within_radius():
    """ testing the function in 1C stations_within_radius"""

    clare_college = (52.2051,0.1152)
    # coords of clare college 
 
    r = 10 

    stations = build_station_list()
    # 'generate the large list of stations and their data 

    stations_in_range = geo.stations_within_radius(stations,clare_college,r)
    # create a list of stations within 10km of clare college and their respsective distance to test on 
    stations_in_range.sort()


    assert type(stations_in_range) == type([])
    assert type(stations_in_range[0][0]) == type("a")
    

    # check that the fuction produces a list where the first element is a string (the station name )


def test_stations_by_river():
    """ testing the 1D function stations by river"""

    stations = build_station_list()

    test = geo.stations_by_river(stations)

    # import dictionary of rivers and their corresponding stations 

    random_number = random.randint(0,len(test)-1)

    # creates a random number within the range of the length of the dictionary 

    assert stations[random_number].name in test[stations[random_number].river]

    # checks if this random station is in the dictionary as the value for the river that it is on (the relevant key)