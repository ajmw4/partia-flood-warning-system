from floodsystem import stationdata
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list

stations = build_station_list()
stationdata.update_water_levels(stations)

for i in stations_level_over_threshold(stations, 0.8):

    print(i[0].name + " " + str(i[1]))

