# Import the relvent functions from their files and folders
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation
from floodsystem.station import inconsistent_typical_range_stations

# 'stations' is a variable which contains the info in build_station_list
stations = build_station_list()

# List of inconsistant stations
inconsistent_range = inconsistent_typical_range_stations(stations)

# Print
print(inconsistent_range)