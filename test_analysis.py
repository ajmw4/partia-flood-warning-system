# Import all of the relevent libraries and modules
import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from floodsystem.station import MonitoringStation
from floodsystem.analysis import polyfit

# Define the test function
def test_polyfit():

    # Set the test values for x as dates one second apart following the function of x^2
    x = [datetime(2020, 2, 20, 0, 0, 1),
        datetime(2020, 2, 20, 0, 0, 2),
        datetime(2020, 2, 20, 0, 0, 3),
        datetime(2020, 2, 20, 0, 0, 4),
        datetime(2020, 2, 20, 0, 0, 5),
        datetime(2020, 2, 20, 0, 0, 6),
        datetime(2020, 2, 20, 0, 0, 7),
        datetime(2020, 2, 20, 0, 0, 8),
        datetime(2020, 2, 20, 0, 0, 9),
        datetime(2020, 2, 20, 0, 0, 10)]

    # Set the test values for y following the function of x^2
    y = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

    # Use the function to work out the test polynomial and test offset
    poly, d0 = polyfit(x, y, 2)

    # For all values of x but thew first
    for n in range(len(x)):
        
        # Test that all of the original values fit on the curve
        assert round(poly(matplotlib.dates.date2num(x[n]) - d0), 2) == y[n]

